## Description

This repository contains a source code of LSM_CA model for relation extraction.

 The LSTM+CA network has three input layers. Two input layers take as input the target entities encoded by the word embeddings. The third input layer takes as input the context between entities, encoded by a word embeddings. The output of all embedding layers fed to LSTM layers, separately. Further, the cross-attention mechanism is applied to the outputs of the LSTM layers. The resulting vectors are concatenated and transferred to the layer with the softmax activation function for classification.

## Requirement

* PyTorch 0.4.0
* NumPy 1.13.3
* tensorboardX 1.2
* Python 3.6

## Model training

```angular2
python train.py
```

Required parameters
```angular2
--model_name // lstm_ca or lstm_ca_feat
--dataset // dataset name
--embed_file // file to pretrained word embedding model
--embed_dim // size of embedding vectors
```

### Add new corpus
1) Create new directory in datasets path, add train.txt, test.txt, train_features.txt and test_features.txt files
2) In train.py  add paths to train and test files in dataset_files dict
3) Count max context len, add to context_dict in train.py file